# qa-service



## Микросервисы
 - [docker-compose.yml](https://gitlab.com/project-silence/qa-service/-/blob/main/docker-compose.yml) - те же сервисы, что в [репозитории с разработкой](https://gitlab.com/project-silence/frequently-questions-model/-/blob/main/docker_services/docker-compose.yml) + app
 - [model_serving/docker/Dockerfile](https://gitlab.com/project-silence/qa-service/-/blob/main/model_serving/docker/Dockerfile) - образ для сервинга модели
 - [model_serving/inference.py](https://gitlab.com/project-silence/qa-service/-/blob/main/model_serving/inference.py) - код сервиса на FastAPI