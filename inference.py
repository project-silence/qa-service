import mlflow
from dotenv import dotenv_values
from fastapi import FastAPI
from prometheus_fastapi_instrumentator import Instrumentator


env_values = dotenv_values("/code/app/.env")
mlflow.set_tracking_uri(f"http://{env_values['MLFLOW_NGINX_NAME']}:{env_values['MLFLOW_NGINX_PWD']}@16.16.194.244:5000/")
mlflow.set_experiment('dummy_model')

app = FastAPI()
Instrumentator().instrument(app).expose(app)


class DummySimilarityModel:
    def __init__(self):
        self.model = mlflow.pyfunc.load_model(env_values['REGISTRY_MODEL_PATH'])

    def predict(self, data):
        return self.model.predict(data)


model = DummySimilarityModel()


@app.post("/invocations")
async def get_pred(data):
    return list(model.predict(data))

